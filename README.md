# sphinx pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/sphinx?branch=main)](https://gitlab.com/buildgarden/pipelines/sphinx/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)

<!-- END BADGIE TIME -->

Build and test [Sphinx](https://www.sphinx-doc.org/en/master/index.html) documentation.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

View the [example
documentation](https://sphinx-buildgarden-pipelines-58dbda7adcbae48cdab58de8cfbf5ba5eb.gitlab.io/).

## Project setup

At a minimum, this pipeline requires:

- Creating a `docs/` directory,

- Running [`sphinx-quickstart`](https://www.sphinx-doc.org/en/master/man/sphinx-quickstart.html) inside the `docs/` directory, and

- Selecting `no` to separate build/source directories.

The `sphinx.ext.coverage` and `sphinx.ext.doctest` extensions must be registered
in the [`docs/conf.py`
file](https://www.sphinx-doc.org/en/master/usage/configuration.html) to use
them.

## Targets

| Name                                  | Include | Hook | Description                                                                                                                 |
| ------------------------------------- | ------- | ---- | --------------------------------------------------------------------------------------------------------------------------- |
| [sphinx-coverage](#sphinx-coverage)   | ✓       |      | Generate a coverage report with [sphinx.ext.coverage](https://www.sphinx-doc.org/en/master/usage/extensions/coverage.html). |
| [sphinx-dirhtml](#sphinx-dirhtml)     | ✓       | ✓    | Build [dirhtml](https://www.sphinx-doc.org/en/master/usage/builders/index.html) HTML pages to serve from a webserver.       |
| [sphinx-doctest](#sphinx-doctest)     | ✓       | ✓    | Test code snippets with [sphinx.ext.doctest](https://www.sphinx-doc.org/en/master/usage/extensions/doctest.html).           |
| [sphinx-linkcheck](#sphinx-linkcheck) | ✓       |      | Verify that URLs are valid with [linkcheck](https://www.sphinx-doc.org/en/master/usage/builders/index.html).                |

## Use cases

### All includes

Add the following to your `.gitlab-ci.yml` file to enable all targets:

```yaml
include:
  - project: buildgarden/pipelines/sphinx
    file:
      - sphinx-dirhtml.yml
      - sphinx-doctest.yml
      - sphinx-coverage.yml
      - sphinx-linkcheck.yml
```
